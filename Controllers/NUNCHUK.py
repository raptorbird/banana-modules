class NUNCHUK():
  def __init__(self, address=0x52):
    self.addr = address
    self.xa = -128
    self.ya = -128
    self.za = -128
    self.x = -128
    self.y = -128
    config = bytearray(2) #create byte array with 2 bytes
    #send over intialization commands
    config[0] = 0x40
    config[1] = 0x00
    i2c.send(config, addr=self.addr)
  def read(self):
    """
    Get raw sensor data from nunchuk

    return    [x-analog, y-analog, x-accel, y-accel, z-accel]
    """
    i2c.send(0x00, addr=self.addr) #tell nunchuk to send back data
    pyb.delay(10) #wait for a bit to let nunchuk catch up
    return i2c.recv(6, addr=self.addr)
  def get_x(self):
    """
    Get x-axis of accelerometer relative to center

    return    range: -127 to 127 (approx)
    """
    return (self.read()[2]+self.xa)
  def get_y(self):
    """
    Get y-axis of accelerometer relative to center

    return    range: -127 to 127 (approx)
    """
    return (self.read()[3]+self.ya)
  def get_z(self):
    """
    Get z-axis of accelerometer relative to center

    return    range: -127 to 127 (approx)
    """
    return (self.read()[4]+self.za)
  def center(self):
    """
    Center the controller's axis
    """
    val = self.read()
    self.x = 0 - val[0]
    self.y = 0 - val[1]
    self.xa = 0 - val[2]
    self.ya = 0 - val[3]
    self.za = 0 - val[4]
  def get_joy_x(self):
    """
    Get the Joystick x-value relative to the center

    return    range: -127 to 127 (approx)
    """
    return (self.read()[0]+self.x)
  def get_joy_y(self):
    """
    Get the Joystick y-value relative to the center

    return    range: -127 to 127 (approx)
    """
    return (self.read()[1]+self.y)
  def get_c(self):
    """
    Get button C of Controller

    return    bool: True = Pressed
                    False = UnPressed
    """
    return not(int(bin(self.read()[5])[-2]))
  def get_z(self):
    """
    Get button Z of Controller

    return    bool: True = Pressed
                    False = UnPressed
    """
    return not(int(bin(self.read()[5])[-1]))
n = NUNCHUK()
