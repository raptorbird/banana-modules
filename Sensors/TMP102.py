class TMP102:
  """Class for TMP102 temperature sensor, implements I2C"""
  def __init__(self, address=0x48):
    """
    Initialize & configure TEMPERATURE Module TMP102
    https://www.sparkfun.com/products/11931

    param    address: i2c address of chip - default=0x48
    """
    self.addr = address
    # Writing Config Values to 0x01
    i2c.mem_write(0x60, self.addr, 0x01)
    i2c.mem_write(0xA0, self.addr, 0x01)
    # Setting Output to 0x00
    i2c.mem_write(0x00, self.addr, 0x00)
  def read(self):
    """
    Reads from TMP102 temperature sensor

    return    temp: returns temperature in degrees (C)
    """
    temp = i2c.recv(2, addr=self.addr)
    return 0.0625*(((temp[0] << 8) + temp[1]) >> 4); #convert data
